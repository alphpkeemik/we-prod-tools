#!/bin/bash
SECONDS=0

echo -e "\e[00;31mwe prod deploy script 1.0.1\e[00m"
source $(dirname "$0")/config.sh
if [ "$REPOSITORY" == '' ] || [ "$BRANCH" == '' ]; then
	echo 'config not init'
	exit;	
fi
TAR_FILE="$BACKUP_FOLDER/$(date '+%Y%m%d_%H%M%S').tar.gz"
echo "backing up to tar "${TAR_FILE}
#exclude ,ust be before
tar --exclude 'var/logs' --exclude 'var/profiler' --exclude 'var/sessions'  --exclude 'var/uploads' -zcf ${TAR_FILE} -C ${DEPLOY_FOLDER} .

rsync -a --delete --exclude '.git/'  --exclude 'var/' ${LOCAL_FOLDER} "$DEPLOY_FOLDER/"
rsync -a  --exclude 'cache/'  --exclude 'logs/'  --exclude 'sessions/'  --exclude 'uploads/' "$LOCAL_FOLDER/var/" "$DEPLOY_FOLDER/var" 

# privileges cache update 1/2
#@TODO move privileges cache update to php level
PP_CF="$DEPLOY_FOLDER/var/cache/we-base-priv-pool.serialized"
PP_CS=''
if [  -f ${PP_CF} ]; then
	PP_CS="$(sha1sum ${PP_CF})"
fi

rm -rf "$DEPLOY_FOLDER/var/cache/*"
rsync -a --delete  "$LOCAL_FOLDER/var/cache/" "$DEPLOY_FOLDER/var/cache" 

cd ${DEPLOY_FOLDER}"/"


make db-update
# privileges cache update 2/2
php bin/console we:priv:reloadpoolcache
#privieges cache
PPC_SN="$(sha1sum ${PP_CF})"

if [ "$PP_CS" != "$PPC_SN" ]; then
  echo "  priv cache not same warming up admin priv cache"
  SECONDS=0
  php bin/console we:admin:privcachewarmup
  duration=$SECONDS
  MSG="  duration $(($duration / 60)) minutes and $(($duration % 60)) seconds"
  echo ${MSG}
fi

#make setup
rm -f var/cache/dev/appDevDebugProject*
if [ "$CRON_ENABLED" == true ]; then
	CRON_CONTENT="$(cat ${DEPLOY_FOLDER}/crontab)"
	CRON_CONTENT="${CRON_CONTENT//__PATH__/$DEPLOY_FOLDER}"
	echo "updating cron content"
	echo "$CRON_CONTENT"
	echo "$CRON_CONTENT" | crontab -
fi

duration=$SECONDS
echo "done in $(($duration / 60)) minutes and $(($duration % 60)) seconds"
if [ "$NOTIFY_EMAIL" != false ]; then
	echo "$NOTIFY_MESSAGE" | mail -a"From: $NOTIFY_FROM" ${NOTIFY_EMAIL}
fi
