#!/bin/bash
SECONDS=0
echo -e "\e[00;31mwe prod build script 1.0.1\e[00m"
source $(dirname "$0")/config.sh

if [ "$LOCAL_FOLDER" == '' ] ||[ "$REPOSITORY" == '' ] || [ "$BRANCH" == '' ]; then
	echo 'config not init'
	exit;
fi

# clone git if not exist
if [ ! -d ${LOCAL_FOLDER} ]; then
	git clone "$REPOSITORY" ${LOCAL_FOLDER}
fi
# to git folder
cd ${LOCAL_FOLDER}

# read current branch
CB=$(git rev-parse --abbrev-ref HEAD)

echo " current branch: $CB"

exists=$(git rev-parse --verify "$BRANCH")

echo " current branch exists: $exists"

# switch to required branch
if [ "$CB" != "$BRANCH" ] && [ -z "$exists" ]
then    
	echo " checkout remote branch: $BRANCH"
	git fetch
	git checkout -b "$BRANCH" "remotes/origin/$BRANCH" --	
fi
if [ "$CB" != "$BRANCH" ] && [ -n "$exists" ]
then    
	echo " checkout locale branch: $BRANCH"
	git checkout "$BRANCH"
fi


CB=$(git rev-parse --abbrev-ref HEAD)
if [ "$CB" != "$BRANCH" ]; then    
	echo " branch $BRANCH not valid"
	exit 1
fi

echo " git pull: $BRANCH"

git pull


rm -rf var/cache/*

${COMPOSER_PATH} install
if [ "$BOWER_ENABLED" = true ] ; then
	php bin/console sp:bower:update
fi
if [ "$MEDIA_ENABLED" = true ] ; then
	php bin/console sonata:classification:fix-context
	php bin/console sonata:media:fix-media-context
fi
make classifier
#@TODO add initial db update needed check here?
php bin/console cache:warmup --env=prod --no-debug
if [ "WEB_RESOURCES_ENABLED" = true ] ; then
	make webr
fi
chmod -R 777 ${LOCAL_FOLDER}"/var"

duration=$SECONDS
echo "done in $(($duration / 60)) minutes and $(($duration % 60)) seconds"
